/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_lc_h.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:16:31 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:16:32 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					apply_mask_lc(t_arg *o, int num_size)
{
	int					i;

	i = 0;
	if (num_size == 1)
		o->content[i] = (char)o->container.d;
	else if (num_size == 2)
	{
		o->content[i] = ((o->container.d >> 6) | MASK_H_TWO);
		o->content[++i] = ((o->container.d & ACC_L_TWO) | MASK_L);
	}
	else if (num_size == 3)
	{
		o->content[i] = ((o->container.d >> 12) | MASK_H_TR);
		o->content[++i] = (((o->container.d & ACC_M_TR) >> 6) | MASK_L);
		o->content[++i] = ((o->container.d & ACC_L_TR) | MASK_L);
	}
	else
	{
		o->content[i] = ((o->container.d >> 18) | MASK_H_F);
		o->content[++i] = (((o->container.d & ACC_N_F) >> 12) | MASK_L);
		o->content[++i] = (((o->container.d & ACC_M_F) >> 6) | MASK_L);
		o->content[++i] = ((o->container.d & ACC_L_F) | MASK_L);
	}
}
