/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_c.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:15:10 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:15:11 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					c_type(t_arg *o, va_list *ap)
{
	unsigned char		tmp;

	o->container.d = va_arg(*ap, int);
	tmp = (unsigned char)o->container.d;
	o->container.s = ft_strnew(1);
	o->container.s[0] = tmp;
	o->dynamic_content = 1;
	o->exact_width = 1;
	o->content = o->container.s;
}

void					c_vtbl(t_arg *o)
{
	o->type = &c_type;
	o->precision = NULL;
	o->width = &s_width;
	o->flags = NULL;
}
