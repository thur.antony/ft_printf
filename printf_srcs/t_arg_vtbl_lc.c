/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_lc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:16:18 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:16:19 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					lc_type(t_arg *o, va_list *ap)
{
	o->container.d = va_arg(*ap, int);
	o->dynamic_content = 1;
}

void					lc_unicode(t_arg *o)
{
	int					size;

	size = get_char_num(get_binary_size(o->container.d));
	o->exact_width = size;
	content_realloc(o, o->exact_width);
	apply_mask_lc(o, size);
	o->container.s = o->content;
}

void					lc_vtbl(t_arg *o)
{
	o->type = &lc_type;
	o->precision = &lc_unicode;
	o->width = &ls_width;
	o->flags = NULL;
}
