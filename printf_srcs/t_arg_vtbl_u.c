/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_u.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:17:47 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:17:48 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					u_type(t_arg *o, va_list *ap)
{
	if (o->bitmap & L)
		o->container.u = va_arg(*ap, unsigned long long);
	else if (o->bitmap & HH)
		o->container.u = (unsigned char)va_arg(*ap, unsigned int);
	else if (o->bitmap & H)
		o->container.u = (unsigned short)va_arg(*ap, unsigned int);
	else if (o->bitmap & L || o->bitmap & J)
		o->container.u = va_arg(*ap, unsigned long);
	else if (o->bitmap & LL)
		o->container.u = va_arg(*ap, unsigned long long);
	else if (o->bitmap & Z)
		o->container.u = va_arg(*ap, size_t);
	else
		o->container.u = va_arg(*ap, unsigned int);
	o->dynamic_content = 1;
	o->content = ft_itoa_base_u_up(o->container.u, 10);
	o->exact_width = ft_strlen(o->content);
}

void					u_precision(t_arg *o)
{
	if (o->bitmap & PRECISION)
		d_prec_modify_pos(o);
}

void					u_vtbl(t_arg *o)
{
	o->type = &u_type;
	o->precision = &u_precision;
	o->flags = NULL;
	o->width = &d_width;
}
