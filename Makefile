# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: avykhova <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/08/26 15:54:20 by avykhova          #+#    #+#              #
#    Updated: 2018/08/26 15:54:24 by avykhova         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

SRC_LIBFTPRINTF = ./printf_srcs/t_*.c \
			./printf_srcs/ft_*.c \
			./libft_srcs/ft_*.c

INCLUDES = -I./includes/
FLAGS = -Wall -Wextra -Werror

all: $(NAME)
	

$(NAME):
	@echo "🥑 compiling libftprintf.a"
	@gcc -c $(SRC_LIBFTPRINTF) $(FLAGS) $(INCLUDES) 
	@ar rc $(NAME) *.o
	@ranlib $(NAME)
	@echo "🥑 ready!"

clean:
	@echo "🥑 cleaning project from 'o' files"
	@/bin/rm -f ./build/*.o
	@/bin/rm -f *.o

fclean: clean
	@echo "🥑 removing project"
	@/bin/rm -f $(NAME)

re: fclean all