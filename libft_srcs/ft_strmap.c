/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/22 16:18:27 by avykhova          #+#    #+#             */
/*   Updated: 2017/12/22 18:49:08 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		size;
	int		i;
	char	*res;

	if (s)
	{
		size = ft_strlen((char *)s) + 1;
		res = (char *)malloc(sizeof(char) * size);
		if (res)
		{
			res[size - 1] = '\0';
			i = 0;
			while (s[i] != '\0')
			{
				res[i] = (*f)(s[i]);
				i++;
			}
			return (res);
		}
	}
	return (NULL);
}
