/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base_unsigned.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 19:01:22 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 19:01:32 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int			length(unsigned long long num, int base)
{
	int			size;

	size = 0;
	if (num == 0)
		return (1);
	while (num)
	{
		size++;
		num = num / base;
	}
	return (size);
}

static char			*apply_base_up(unsigned long long num, int base, int size)
{
	int				i;
	char			*res;

	res = ft_strnew_mod(size, '<');
	i = size;
	if (num == 0)
		res[0] = '0';
	while (num)
	{
		res[--i] = NUM_UP[num % base];
		num = num / base;
	}
	return (res);
}

static char			*apply_base_low(unsigned long long num, int base, int size)
{
	int				i;
	char			*res;

	res = ft_strnew_mod(size, '<');
	i = size;
	if (num == 0)
		res[0] = '0';
	while (num)
	{
		res[--i] = NUM_LOW[num % base];
		num = num / base;
	}
	return (res);
}

char				*ft_itoa_base_u_up(unsigned long long num, int base)
{
	char			*res;
	int				size;

	size = length(num, base);
	res = apply_base_up(num, base, size);
	return (res);
}

char				*ft_itoa_base_u_low(unsigned long long num, int base)
{
	char			*res;
	int				size;

	size = length(num, base);
	res = apply_base_low(num, base, size);
	return (res);
}
