/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 13:09:34 by avykhova          #+#    #+#             */
/*   Updated: 2017/12/18 13:09:35 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char	*src_p;
	unsigned char	*dst_p;

	src_p = (unsigned char *)src;
	dst_p = (unsigned char *)dest;
	while (n--)
	{
		*dst_p = *src_p;
		dst_p++;
		if (*src_p == (unsigned char)c)
			return ((void *)dst_p);
		src_p++;
	}
	return (NULL);
}
