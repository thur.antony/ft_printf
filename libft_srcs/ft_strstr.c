/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 13:12:27 by avykhova          #+#    #+#             */
/*   Updated: 2018/01/04 15:42:13 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s, const char *ss)
{
	size_t	i;
	size_t	s_i;
	size_t	sub_len;

	i = 0;
	s_i = 0;
	sub_len = ft_strlen(ss);
	if (!ft_strlen(ss))
		return ((char *)s);
	while (s[i] != '\0')
	{
		while (s[i + s_i] == ss[s_i] && s[i + s_i] && s[s_i])
			s_i++;
		if (s == ss)
			return ((char *)s);
		if (s_i == sub_len)
			return (&((char *)s)[i]);
		else
		{
			s_i = 0;
			i++;
		}
	}
	return (NULL);
}
