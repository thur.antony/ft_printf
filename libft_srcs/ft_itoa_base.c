/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 15:24:31 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 15:24:35 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int			length(long long num, int base)
{
	int				size;
	int				unsign;

	size = 0;
	unsign = (num > 0);
	if (num == 0)
		return (1);
	if (!unsign)
	{
		size++;
		num = (-1) * num;
	}
	while (num)
	{
		size++;
		num = num / base;
	}
	return (size);
}

static char			*apply_base(long long num, int base, int size)
{
	int				i;
	char			*res;

	res = ft_strnew_mod(size, '<');
	i = size;
	if (num == 0)
		res[0] = '0';
	else if (num < 0)
	{
		if (base == 10)
			res[0] = '-';
		num = (-1) * num;
	}
	while (num)
	{
		res[--i] = NUM[num % base];
		num = num / base;
	}
	return (res);
}

char				*ft_itoa_base(long long num, int base)
{
	char			*res;
	int				size;

	if (num < -9223372036854775807)
		return (ft_strdup("-9223372036854775808"));
	size = length(num, base);
	res = apply_base(num, base, size);
	return (res);
}
