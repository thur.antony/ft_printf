/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/22 16:20:59 by avykhova          #+#    #+#             */
/*   Updated: 2017/12/22 18:27:43 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	size_s[2];
	size_t	size;
	size_t	i;
	char	*str_joined;

	if (s1 && s2)
	{
		size_s[0] = ft_strlen((char *)s1);
		size_s[1] = ft_strlen((char *)s2);
		size = size_s[0] + size_s[1];
		i = -1;
		str_joined = (char *)malloc(sizeof(char) * (size + 1));
		if (str_joined)
		{
			str_joined[size] = '\0';
			while (++i < size_s[0])
				str_joined[i] = s1[i];
			i = -1;
			while (++i < size_s[1])
				str_joined[size_s[0] + i] = s2[i];
			return (str_joined);
		}
	}
	return (NULL);
}
