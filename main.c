#include <stdio.h>
#include "includes/ft_printf.h"

int     main(void)
{
    int     m;
    int     n;
	


	m = printf("%\n");
	n = ft_printf("%\n");
	ft_printf("printf:%6d\nft_printf:%3d\n\n", m, n);

	m = printf("{%03c}\n", 0);
	n = ft_printf("{%03c}\n", 0);
	ft_printf("printf:%6d\nft_printf:%3d\n\n", m, n);

	m = printf("%.0p, %.p\n", 0, 0);
	n = ft_printf("%.0p, %.p\n", 0, 0);
	ft_printf("printf:%6d\nft_printf:%3d\n\n", m, n);

	m = printf("%.p, %.0p\n", 0, 0);
	n = ft_printf("%.p, %.0p\n", 0, 0);
	ft_printf("printf:%6d\nft_printf:%3d\n\n", m, n);

	m = printf("%.4S\n", L"ÊM-M-^QÊM-^XØ‰∏M-ÂM-^O™ÁM-^L´„M-M-^B");
	n = ft_printf("%.4S\n", L"ÊM-M-^QÊM-^XØ‰∏M-ÂM-^O™ÁM-^L´„M-M-^B");
	ft_printf("printf:%6d\nft_printf:%3d\n\n", m, n);

	m = printf("{%-15Z}\n", 123);
	n = ft_printf("{%-15Z}\n", 123);
	ft_printf("printf:%6d\nft_printf:%3d\n\n", m, n);

	m = printf("%.4d\n", -424242);
	n = ft_printf("%.4d\n", -424242);
	ft_printf("printf:%6d\nft_printf:%3d\n\n", m, n);


	//ft_printf("%u\n", 4294967297);printf("%u\n", 4294967297);
    // printf("\n");
    // m = printf("%.p, %.0p\n", 0, 0);
    // n = ft_printf("%.p, %.0p\n", 0, 0);
    // ft_printf("printf:%6d\nft_printf:%3d\n", m, n);

	// int		i;
	// setlocale(LC_ALL, "en_US.UTF-8");

	// // ft_printf("\n\n");
	// // i = printf("|%#x|\n", 42);
	// // m = ft_printf("|%#x|\n", 42);
	// // printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// // ft_printf("\n\n");
	// // i = printf("|%#llx|\n", 9223372036854775807);
	// // m = ft_printf("|%#llx|\n", 9223372036854775807);
	// // printf("printf:%6d\nft_printf:%3d\n\n", i, m);
	
	// // ft_printf("\n\n");
	// // i = printf("|%#X|\n", 42);
	// // m = ft_printf("|%#X|\n", 42);
	// // printf("printf:%6d\nft_printf:%3d\n\n", i, m);
	
	// // ft_printf("\n\n");
	// // i = printf("|%#8x|\n", 42);
	// // m = ft_printf("|%#8x|\n", 42);
	// // printf("printf:%6d\nft_printf:%3d\n\n", i, m);
	
	// // ft_printf("\n\n");
	// // i = printf("|%#-08x|\n", 42);
	// // m = ft_printf("|%#-08x|\n", 42);
	// // printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("\n\n");
	// i = printf("@moulitest: %5.d %5.0d\n", 0, 0);
	// m = ft_printf("@moulitest: %5.d %5.0d\n", 0, 0);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);
	

	// // ft_printf("@moulitest: %.o %.0o", 0, 0);
	// ft_printf("@moulitest: %5.o %5.0o", 0, 0);
	// ft_printf("@moulitest: %#.o %#.0o", 0, 0);
	// ft_printf("%zd", -1);
	// ft_printf("@moulitest: %.d %.0d", 42, 43);
	// ft_printf("@moulitest: %.d %.0d", 0, 0);
	// ft_printf("@moulitest: %5.d %5.0d", 0, 0);
	// ft_printf("%hU", 4294967296);


	// ft_printf("\n-------- INT MIN check --\n\n");
	// //simple
	// i = printf("|%x|\n", -42);
	// m = ft_printf("|%x|\n", -42);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// i = printf("|%u|\n", -42);
	// m = ft_printf("|%u|\n", -42);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("\n-------- INT MIN check --\n\n");
	// //simple
	// i = printf("|%ld|\n", -2147483648);
	// m = ft_printf("|%ld|\n", -2147483648);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);
	
	// ft_printf("\n-------- INT MAX check --\n\n");
	// //simple
	// i = printf("|%d|\n", 2147483647);
	// m = ft_printf("|%d|\n", 2147483647);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("\n-------- UINT MAX check --\n\n");
	// //simple
	// i = printf("|%ld|\n", 4294967295);
	// m = ft_printf("|%ld|\n", 4294967295);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("\n-------- long min check --\n\n");
	// //simple
	// i = printf("|%lld|\n", -9223372036854775808);
	// m = ft_printf("|%lld|\n", -9223372036854775808);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("\n-------- long max check --\n\n");
	// //simple
	// i = printf("|%lld|\n", 9223372036854775807);
	// m = ft_printf("|%lld|\n", 9223372036854775807);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("\n-------- long max check --\n\n");
	// //simple
	// i = printf("|%llu|\n", 18446744073709551615);
	// m = ft_printf("|%llu|\n", 18446744073709551615);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("\n-------- long max check --\n\n");
	// //simple
	// i = printf("|%llu|\n", 18446744073709551615);
	// m = ft_printf("|%llu|\n", 18446744073709551615);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("\n-------- long max check --\n\n");
	// //simple
	// i = printf("|%llu|\n", 18446744073709551615);
	// m = ft_printf("|%llu|\n", 18446744073709551615);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("\n-------- size_t check --\n\n");
	// //simple
	// i = printf("|%zd|\n", 18446);
	// m = ft_printf("|%zd|\n", 18446);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	

	// ft_printf("\n-------- UNSIGNED INT check --\n\n");
	// //simple
	// i = printf("|%#o|\n", 17);
	// m = ft_printf("|%#o|\n", 17);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// //prec
	// i = printf("|%#6o|\n", 17);
	// m = ft_printf("|%#6o|\n", 17);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// //width
	// i = printf("|%#05o|\n", 1);
	// m = ft_printf("|%#05u|\n", 1);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// //flags
	// i = printf("|%#-5lo|\n", 3789789781);
	// m = ft_printf("|%#-5lo|\n", 3789789781);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// i = printf("|%#10o|\n", 'a');
	// m = ft_printf("|%#10o|\n", 'a');
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);


	// ft_printf("\n-------- INT check --\n\n");
	// //simple
	// i = printf("|% d|\n", 17);
	// m = ft_printf("|% d|\n", 17);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// //prec
	// i = printf("|% 06d|\n", 17);
	// m = ft_printf("|% 06d|\n", 17);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// //width
	// i = printf("|% 05zd|\n", 17);
	// m = ft_printf("|% 05d|\n", 17);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// //flags
	// i = printf("|%-5ld|\n", 3789789781);
	// m = ft_printf("|%-5ld|\n", 3789789781);
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// i = printf("|% 10d|\n", 'a');
	// m = ft_printf("|% 10d|\n", 'a');
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// setlocale(LC_ALL, "en_US.UTF-8");

	// ft_printf("-------- STRING check --\n\n");
	// i = printf("|%s|\n|%.10s|\n|%.3s|\n|%10s|\n|%-10s|\n|%-3s|\n|%-10.3s|\n|%10.4s|\n\n", "0123456789", "0123456789", "0123456789", "0123456789", "0123456789", "0123456789", "0123456789", "0123456789");
	// m = ft_printf("|%s|\n|%.10s|\n|%.3s|\n|%10s|\n|%-10s|\n|%-3s|\n|%-10.3s|\n|%10.4s|\n\n", "0123456789", "0123456789", "0123456789", "0123456789", "0123456789", "0123456789", "0123456789", "0123456789");
	// printf("printf:%6d\nft_printf:%3d\n", i, m);

	// ft_printf("\n-------- WIDE STRING check --\n\n");
	// printf("|%-20S|\n", L"🥑");
	// ft_printf("|%-20S|\n", L"🥑");
	// i = printf("|%20S|\n", L"🥑");
	// m = ft_printf("|%20S|\n", L"🥑");
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("-------- WIDE STRING check --\n\n");
	// printf("|%-20S|\n", L"Θ");
	// ft_printf("|%-20S|\n", L"Θ");
	// i = printf("|%20S|\n", L"α");
	// m = ft_printf("|%20S|\n", L"α");
	// printf("printf:%6d\nft_printf:%3d\n", i, m);
	// i = printf("|%S|\n", L"α");
	// m = ft_printf("|%S|\n", L"α");
	// printf("printf:%6d\nft_printf:%3d\n\n", i, m);

	// ft_printf("\n-------- CHAR check --\n\n");
	// printf("|%c|\n", 'A');
	// ft_printf("|%c|\n", 'A');
	// printf("|%3c|\n", 'A');
	// ft_printf("|%3c|\n", 'A');
	// i = printf("|%-3c|\n", 'A');
	// m = ft_printf("|%-3c|\n", 'A');
	// printf("printf:%6d\nft_printf:%3d\n", i, m);

	// ft_printf("\n-------- WIDE CHAR check --\n\n");
	// printf("|%C|\n", L'α');
	// ft_printf("|%C|\n", L'α');
	// printf("|%3C|\n", L'α');
	// ft_printf("|%3C|\n", L'α');
	// i = printf("|%-3C|\n", L'α');
	// m = ft_printf("|%-3C|\n", L'α');
	// printf("printf:%6d\nft_printf:%3d\n", i, m);

	// ft_printf("\n-------- above MIX check --\n\n");
	// printf("hello, i'm str :%s;\nhello, i'm wstr:%S;\nhello, i'm char:%c;\nhello, i'm wchr:%C;\n", "string", L"α🥑α🥑a", 'a', L'🥑');
	// ft_printf("hello, i'm str :%s;\nhello, i'm wstr:%S;\nhello, i'm char:%c;\nhello, i'm wchr:%C;\n", "string", L"α🥑α🥑a", 'a', L'🥑');

	return (0);
}